import tkinter as tk
from tkinter import filedialog
from tkinter.scrolledtext import ScrolledText
from tkinter import ttk
from ctypes import windll
windll.shcore.SetProcessDpiAwareness(1)

def find_occurrences(s, ch):
    return [i for i, letter in enumerate(s) if letter == ch]

class Main_window():
    def __init__(self, file_dict, constant_list):
        self.root = tk.Tk()
        self.screen_width = self.root.winfo_screenwidth()
        self.screen_heigh = self.root.winfo_screenheight()
        # Set size and position of variable window
        self.root.geometry(f"{self.screen_width}x{self.screen_heigh-80}+{0}+{0}")
        self.root.resizable(False, False)
        
        self.font = "Default"
        self.font_size = 16
        
        self.create_menu()
        
        self.file_paths = [] # To get the paths of the files sorted
        self.frame_list = []
        self.text_editor_list = [] # List with the scrolled text widgets
        self.file_dict = file_dict
        self.create_notebook(self.file_dict)
        
        self.constants = constant_list
        self.create_constants_listbox(self.constants)
        
        self.create_buttons()
    
    def create_menu(self):
        self.menubar = tk.Menu(self.root)
        self.root.config(menu=self.menubar)
        
        file_menu = tk.Menu(self.menubar, tearoff=0)
        file_menu.add_command(label="Import", command=self.import_command)
        file_menu.add_command(label="Open", command=self.open_command)
        file_menu.add_command(label="Close", command=self.close_command)
        file_menu.add_command(label="Close all", command=self.close_all_command)
        file_menu.add_separator()
        file_menu.add_command(label="Exit", command=self.root.destroy)
        self.menubar.add_cascade(label="File", menu=file_menu)
        
        help_menu = tk.Menu(self.menubar, tearoff=0)
        help_menu.add_command(label="About", command=self.help_command)
        self.menubar.add_cascade(label="Help", menu=help_menu)
        
    def import_command(self):
        self.constants = import_constants()
        self.c_listbox.delete(0, tk.END)
        for c in self.constants:
            self.c_listbox.insert('end', c)

    def open_command(self):
        # TODO
        pass
        
    def close_command(self):
        frame_id = self.notebook.select()
        index = self.notebook.index(frame_id)
        self.frame_list.pop(index)
        self.text_editor_list.pop(index)
        file_path = self.file_paths.pop(index)
        del self.file_dict[file_path]
        self.notebook.forget(frame_id)
        
    def close_all_command(self):
        tab_id = self.notebook.select()
        while tab_id:
            self.notebook.forget(tab_id)
            tab_id = self.notebook.select()
        self.frame_list[:] = []
        self.text_editor_list[:] = []
        self.file_paths[:] = []
        self.file_dict.clear()
    
    def help_command(self):
        # TODO
        pass
    
    def create_notebook(self, file_dict):
        self.notebook = ttk.Notebook(self.root)
        self.notebook.place(relx=0, rely = 0, relwidth=0.6, relheigh=0.99)
        for i, file in enumerate(file_dict.keys()):
            self.frame_list.append(ttk.Frame(self.notebook, width = self.screen_width / 2, heigh = self.screen_heigh, relief = tk.SUNKEN))
            self.notebook.add(self.frame_list[i], text=file[find_occurrences(file,'/')[-1] + 1:])
            self.text_editor_list.append(ScrolledText(self.frame_list[i], wrap = tk.WORD))
            self.text_editor_list[i].place(relx=0, rely=0, relwidth=1, relheigh=1)
            self.text_editor_list[i].insert(tk.END, file_dict[file])
        self.file_paths = list(self.file_dict.keys())
    
    def create_constants_listbox(self, constant_list):
        l1 = tk.Label(self.root, text="Constants", font=(self.font, self.font_size), anchor="w")
        l1.place(relx=0.7, rely=0, relwidth=0.3, relheigh = 0.05)
        var = tk.Variable(value=constant_list)
        self.c_listbox = tk.Listbox(self.root, listvariable=var)
        self.c_listbox.place(relx=0.7, rely = 0.05, relwidth=0.29, relheigh=0.94)
        self.c_scrollbar = ttk.Scrollbar(self.root, orient=tk.VERTICAL, command=self.c_listbox.yview)
        self.c_listbox["yscrollcommand"] = self.c_scrollbar.set
        self.c_scrollbar.place(relx=0.99, rely=0.05, relwidth=0.01, relheigh=0.94)
        self.c_listbox.bind("<<ListboxSelect>>", self.item_selected)
    
    def item_selected(self, event):
        c_idx = self.c_listbox.curselection()
        if c_idx:
            constant = self.c_listbox.get(c_idx)
    
    def create_buttons(self):
        self.next_b = ttk.Button(self.root, text = "Next", command=self.next_command)
        self.next_b.place(relx=0.62, rely=0.5, relwidth=0.06, relheigh=0.03)
        
        self.previous_b = ttk.Button(self.root, text = "Previous", command=self.previous_command)
        self.previous_b.place(relx=0.62, rely=0.6, relwidth=0.06, relheigh=0.03)
        
        self.replace_b = ttk.Button(self.root, text = "Replace", command=self.replace_command)
        self.replace_b.place(relx=0.62, rely=0.7, relwidth=0.06, relheigh=0.03)
        
        self.replace_all_b = ttk.Button(self.root, text = "Replace all", command=self.replace_all_command)
        self.replace_all_b.place(relx=0.62, rely=0.8, relwidth=0.06, relheigh=0.03)
        
    def next_command(self):
        # TODO
        pass
       
    def previous_command(self):
        # TODO
        pass
        
    def replace_command(self):
        # TODO
        pass
    
    def replace_all_command(self):
        # TODO
        pass
    
    def run(self):
        self.root.mainloop()

def import_constants():
    import_window = tk.Tk()
    import_window.withdraw()
    input_file = filedialog.askopenfilename(title = "Select constant file")
    if not input_file:
        return []
    content = ""
    with open(input_file, 'r') as in_f:
        content = in_f.readlines()
    constant_list = []
    for line in content:
        constant_info = line.split(',')
        if constant_info[0] == "_DEFINED_":
            constant_list.append(constant_info[2] + " (" + constant_info[3] + ")")
    constant_list.sort()
    import_window.destroy()
    return constant_list
            
def open_files():
    open_window = tk.Tk()
    open_window.withdraw()
    input_files = filedialog.askopenfilenames(title = "Select code files")
    if not input_files:
        return dict()
    file_dict = {f: "" for f in input_files}
    for f in file_dict.keys():
        with open(f, 'r') as in_f:
            file_dict[f] = in_f.read()
    open_window.destroy()
    return file_dict
    
   
def main():
    constants_list = import_constants()
    file_dict = open_files()
    window = Main_window(file_dict, constants_list)
    window.run()
    
if __name__ == "__main__":
    main()