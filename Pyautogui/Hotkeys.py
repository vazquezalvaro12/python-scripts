from time import sleep
import pyautogui
import keyboard

IsaGraf_Positions = {"File": (38,31), "Import":(131,241), "Export":(131,271), "Plc_definition":(343,243), 
                    "Exange_file":(343,276), "Variables_i":(343,304), "Resource":(360,333), 
                    "Entire_Resource":(514,333), "Variables_e":(343, 391), "POU": (343,361)}

Forticlient_Positions = {"Password": (982,633)}

# ctrl+alt+i
def import_structure():
    pyautogui.moveTo(IsaGraf_Positions["File"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Import"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Plc_definition"])
    pyautogui.click()

#ctrl+alt+e
def import_exange_file():
    pyautogui.moveTo(IsaGraf_Positions["File"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Import"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Exange_file"])
    pyautogui.click()

#ctrl+alt+v
def import_variables():
    pyautogui.moveTo(IsaGraf_Positions["File"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Import"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Variables_i"])
    pyautogui.click()

#ctrl+alt+r
def export_resource():
    pyautogui.moveTo(IsaGraf_Positions["File"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Export"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Resource"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Entire_Resource"])
    pyautogui.click()

#ctrl+alt+shift+v
def export_variables():
    pyautogui.moveTo(IsaGraf_Positions["File"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Export"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Variables_e"])
    pyautogui.click()

#ctrl+alt+p
def export_POU():
    pyautogui.moveTo(IsaGraf_Positions["File"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["Export"])
    pyautogui.click()
    pyautogui.moveTo(IsaGraf_Positions["POU"])
    pyautogui.click()

#ctrl+alt+f
def vpn_password():
    pyautogui.moveTo(Forticlient_Positions["Password"])
    pyautogui.click()
    sleep(0.1)
    pyautogui.typewrite("BdA9zqsCRA", interval=0.02)
    pyautogui.typewrite(["enter"])

def main():
    hotkeys = {"ctrl+alt+i":import_structure, "ctrl+alt+e":import_exange_file, "ctrl+alt+v":import_variables,
    "ctrl+alt+r": export_resource, "ctrl+alt+shift+v":export_variables, "ctrl+alt+p":export_POU, "ctrl+alt+f":vpn_password}
    for hotkey, f in hotkeys.items():
        keyboard.add_hotkey(hotkey,f)
    try:
        keyboard.wait("ctrl+shift+q")
    except KeyboardInterrupt:
        print("User interruption")
        return

if __name__ == "__main__":
    main()