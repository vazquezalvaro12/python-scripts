from time import sleep
from turtle import distance
import pyautogui

def main():
    sleep(10)
    try:
        while True:
            w,h = pyautogui.size()
            pyautogui.typewrite(['down', 'enter', 'down', 'down','enter'], interval=0.2)
    except KeyboardInterrupt:
        print("User interruption")
        return

if __name__ == "__main__":
    main()