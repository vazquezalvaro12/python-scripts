import pandas as pd
import tkinter
from tkinter import filedialog
import sys

# Constants
TEXT_DESCRIPTION = "Texto Descriptivo / Text Description"
REQ_ID = "ID"
CATEGORY = "Categoría / Category"

def ask_open_excel_file():
    tkinter.Tk().withdraw() # prevents an empty tkinter window from appearing
    input_file = filedialog.askopenfilename(title = "Select diagnostic file", defaultextension='.xlsx')
    return input_file

def main():
    in_f = ask_open_excel_file()
    if in_f:
        print("File selected " + in_f)
    else:
        print("No file selected.")
        sys.exit()

    df_diag = pd.read_excel(in_f)
    diag_code = ""

    for i in range(0, len(df_diag.index), 2):
        req_id = (df_diag.iloc[i][REQ_ID], df_diag.iloc[i + 1][REQ_ID])
        text = (df_diag.iloc[i][TEXT_DESCRIPTION], df_diag.iloc[i + 1][TEXT_DESCRIPTION])
        category = (df_diag.iloc[i][CATEGORY], df_diag.iloc[i + 1][CATEGORY])
        text_lines = [[ line for line in text[0].split('\n') if line != ""],[line for line in text[1].split('\n') if line != ""]] 
        
        # Extract variable name
        var_name = [text_lines[0][text_lines[0].index('WHEN') + 1], text_lines[1][text_lines[1].index('WHEN') + 1]]
        equal = [var_name[0].find('='), var_name[1].find('=')]
        var_value = [var_name[0][equal[0] + 1:], var_name[1][equal[1] + 1:]]
        var_value = [var_value[0].replace(" ",""), var_value[1].replace(" ","")]
        var_name = [var_name[0][:equal[0]], var_name[1][:equal[1]]]
        var_name = [var_name[0].replace(" ",""), var_name[1].replace(" ","")]
        # Extract diagnostic name
        diagnostic_name = [text_lines[0][text_lines[0].index('THEN') + 1], text_lines[1][text_lines[1].index('THEN') + 1]]

        diagnostic_value = [diagnostic_name[0].split(" ")[-1], diagnostic_name[1].split(" ")[-1]]
        diagnostic_value = [diagnostic_value[0].replace(" ",""), diagnostic_value[1].replace(" ","")]
        diagnostic_name = [diagnostic_name[0].split(" ")[2], diagnostic_name[1].split(" ")[2]]
        diagnostic_name = [diagnostic_name[0].replace(" ",""), diagnostic_name[1].replace(" ","")]

        # Check parity of diagnostics
        not_str = ""
        if var_name[0] != var_name[1]:
            print(f"Requirements for diagnostics {req_id[0]} and {req_id[1]} refer to different variables {var_name[0]} and {var_name[1]}")
            continue
        if diagnostic_name[0] != diagnostic_name[1]:
            print(f"Requirements for diagnostics {req_id[0]} and {req_id[1]} refer to different diagnostics {diagnostic_name[0]} and {diagnostic_name[1]}")
            continue

        # Check not str
        var_descending = False
        diag_descending = False
        if ( var_value[0] == "1" or var_value[0].upper() == "TRUE") and ( var_value[1] == "0" or var_value[1].upper() == "FALSE"):
            var_descending = True
        if ( diagnostic_value[0] == "1" or diagnostic_value[0].upper() == "TRUE") and ( diagnostic_value[1] == "0" or diagnostic_value[1].upper() == "FALSE"): 
            diag_descending = True
        
        if ( var_descending and not diag_descending ) or ( not var_descending and diag_descending ):
            not_str = "NOT " # Diagnostic behaviour is opposed to the variable
        
        # Register code
        diag_code += (f"\t(* [+] {req_id[0]}, {req_id[1].split('-')[-1]} *)\n")
        diag_code += (f"\t(* o_o{diagnostic_name[0]} := {not_str}i_{var_name[0]}; *)\n")
        diag_code += (f"\t(* [.] {req_id[0]}, {req_id[1].split('-')[-1]} *)\n")
        diag_code += (f"\n")
    with open("Code.st", 'w') as f_out:
        f_out.write(diag_code)
if __name__ == "__main__":
    main()