DESCRIPCIÓN
	Programa para exportar estructuras de Isagraf.
	
INSTRUCCIONES DE USO
	1. Imprima los tipos del proyecto. Para ello vaya a File -> Print
	2. En la ventana Document Generator en table marque únicamente Types.
	3. En la ventana Document Generator en options marque print to file y
	en header/footer marque None. Seleccione la ubicación donde guardar el archivo.
	4. Guardar el documento como txt
	4. Vaya donde tenga instalado el programa.
	5. Ejecutar (o ejecutar el exe directamente)
		python3 main.py
	6. Seleccione el fichero de entrada.