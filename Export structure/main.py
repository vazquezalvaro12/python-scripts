import sys
import tkinter
from tkinter import filedialog


def create_structure(raw_text):
    # Get name of the structure file
    structure_name = "Structure_" + raw_text[0][:-1] + '.TDB' # avoid \n
    with open(structure_name, 'w', encoding='utf-8') as f:
        # Write header of structure.
        f.write("[targetUpdate] LINUX_EKE\n[end]\n\n")
        f.write("[structure] " + raw_text[0] + "[end]\n\n")
        for i in range(2, len(raw_text), 4):
            if raw_text[i].find('\n',0,1) != -1:
                break
            if i < len(raw_text) - 1:
                if raw_text[i+2].find('\n',0,1) != -1:
                    break
            field = raw_text[i]
            type = raw_text[i+2][6:]
            comment = None
            string_length = None
            if '(' in field: # there is a comment
                comment_start = field.find('(')
                comment = field[comment_start + 3:-4]
                field = field[:comment_start - 1] + '\n'
            if '(' in type: # type has length
                string_length_start = type.find('(')
                string_length = type[string_length_start + 1:-2]
                type = type[:string_length_start] + '\n'
            f.write("[field] " + field + "Type=" + type)
            if string_length:
                f.write("StringLength=" + string_length + '\n')
            if comment:
                f.write("SingleTextLine=" + comment + '\n')
            f.write("[end]\n\n")
            
def create_array(raw_text_lines):
    comment_start = raw_text_lines[0].find('(')
    # Get name of the array file
    array_name = raw_text_lines[0][:comment_start] + ".TDB" # If there is no comment comment_start = -1 avoid \n
    with open(array_name, 'w', encoding='utf-8') as f:
        # Write header of structure
        f.write("[targetUpdate] LINUX_EKE\n[end]\n\n")
        f.write("[array] " + raw_text_lines[0][:comment_start] + "\n")
        type = raw_text_lines[2][6:]
        comment = ''
        if comment_start != -1: # There is a comment
            comment = raw_text_lines[0][comment_start + 3:-4]
        dimension = raw_text_lines[4][11:]
        f.write(f"Type={type}")
        f.write(f"Dimension={dimension}")
        if comment:
            f.write(f"SingleTextLine={comment}\n")
        f.write("[end]\n")
        

if __name__ == "__main__":
    print("Select file.")
    tkinter.Tk().withdraw() # prevents an empty tkinter window from appearing
    input_file = filedialog.askopenfilename(title = "Seleccione fichero de entrada")
    if input_file:
        print("File selected " + input_file)
        print("Starting export...\n")
    else:
        print("No file selected.")
        sys.exit()
    content = ""
    try:
        with open(input_file, 'r', encoding="utf-8") as in_f:
            content = in_f.readlines()
        for i in range(0, len(content)):
            content[i] = content[i].replace('     ', '') # delete tabs
        structure_start = []
        array_start = []
        # Check where start the structures or arrays ----------
        for i in range(0, len(content)): 
            if content[i].find("ST_", 0, 3) != -1:
                structure_start.append(i)
            elif content[i].find("AR", 0, 2) != -1:
                array_start.append(i)

        # Create the structures ------------------------------
        for i in range(0, len(structure_start)): 
            if i == len(structure_start) - 1: # last structure
                create_structure(content[structure_start[i]:])
            else:
                create_structure(content[structure_start[i]:structure_start[i+1] - 1])

        # Create the arrays ----------------------------------
        for start in array_start:
            create_array(content[start:start+5])

        print(f"Export finished. Found {len(structure_start)} structures and {len(array_start)} arrays in file.\n")

    except FileNotFoundError:
        if input_file:
            print(input_file + " not found.")
    except UnicodeDecodeError as e:
        print("Error: " + str(e))
    except IndexError:
        print("The file has not the correct format.")