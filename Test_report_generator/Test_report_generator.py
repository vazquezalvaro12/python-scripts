import pandas as pd
import numpy as np
import tkinter
from tkinter import filedialog
import sys
import datetime


def ask_open_excel_file():
    tkinter.Tk().withdraw() # prevents an empty tkinter window from appearing
    input_file = filedialog.askopenfilename(title = "Select SRS", defaultextension='.xlsx')
    if input_file:
        print("File selected " + input_file)
    else:
        print("No file selected.")
        sys.exit()
    return input_file

def highlight(x):
    color = "background-color: #0be6de"
    c = np.where(x['Test date'] == "", color, None)
    y = pd.DataFrame("", index=x.index, columns=x.columns)
    for column in x.columns:
        y[column] = c
    return y

def main():
    in_f = ask_open_excel_file()
    df_srs = pd.read_excel(in_f)
    columns = []
    VCU_CH = False
    if 'VCUx_PH' in df_srs.columns:
        columns = ['Test date', 'Module', 'Progress', 'Test ID', 'SW-0658-REQ', 'Test description',
                                    'Initial Conditions', 'Results', 'Status', 'Comment']
    else:
        columns = ['Test date', 'Module', 'Progress', 'Test ID', 'SW-0659-REQ', 'Test description',
                                    'Initial Conditions', 'Results', 'Status', 'Comment']
        VCU_CH = True
    dict_test_result = {column:[] for column in columns} # For VCU_CH and VCU_PH1
    dict_test_result_2 = {column:[] for column in columns} # For VCU_PH2
        
    # get current date
    date = datetime.datetime.today().strftime("%d-%m-%Y")
    
    module = df_srs["Texto Descriptivo / Text Description"][0]
    idx_start = 0
    
    for i in range(0, len(df_srs.index)):
        if df_srs.iloc[i]["Texto Descriptivo / Text Description"] == "Software requirements":
            idx_start = i + 1
            break
    test_id = 1 # For VCU_CH and VCU_PH1
    test_id_ph2 = 1 # For VCU_PH2
    at_least_one_req_ph1 = False
    at_least_one_req_ph2 = False
    if VCU_CH:
        for i in range(idx_start, len(df_srs.index)):
            if pd.isna(df_srs.iloc[i]["Categoría / Category"]) and \
                not "Obsolete" in df_srs.iloc[i]["Texto Descriptivo / Text Description"] and \
                not "Parameters list" in df_srs.iloc[i]["Texto Descriptivo / Text Description"]: # Chapter title
                dict_test_result["Test date"].append("")
                dict_test_result["Module"].append(module)
                dict_test_result["Progress"].append("")
                dict_test_result["Test ID"].append("")
                dict_test_result["SW-0659-REQ"].append(int(df_srs.iloc[i]["ID"][12:]))
                dict_test_result["Test description"].append(df_srs.iloc[i]["Texto Descriptivo / Text Description"])
                dict_test_result["Initial Conditions"].append("")
                dict_test_result["Results"].append("")
                dict_test_result["Status"].append("")
                dict_test_result["Comment"].append("")

            elif df_srs.iloc[i]["Categoría / Category"] == 'P': # Requirement
                dict_test_result["Test date"].append(date)
                dict_test_result["Module"].append(module)
                dict_test_result["Progress"].append(test_id)
                dict_test_result["Test ID"].append(test_id)
                dict_test_result["SW-0659-REQ"].append(int(df_srs.iloc[i]["ID"][12:]))
                
                description = df_srs.iloc[i]["Texto Descriptivo / Text Description"]
                given_starts = description.find("GIVEN")
                then_starts = description.find("THEN")
                test_description = ""
                if given_starts != 0: # There is a test description
                    test_description = description[:given_starts - 1]
                initial_conditions = description[given_starts:then_starts]
                results = description[then_starts + 5:]
                
                dict_test_result["Test description"].append(test_description)
                dict_test_result["Initial Conditions"].append(initial_conditions)
                dict_test_result["Results"].append(results)
                dict_test_result["Status"].append("In progress")
                dict_test_result["Comment"].append("")

                test_id += 1
    else:
        for i in range(idx_start, len(df_srs.index)):
            if pd.isna(df_srs.iloc[i]["Categoría / Category"]) and \
                not "Obsolete" in df_srs.iloc[i]["Texto Descriptivo / Text Description"] and \
                not "Parameters list" in df_srs.iloc[i]["Texto Descriptivo / Text Description"] and \
                not "Parameters table" in df_srs.iloc[i]["Texto Descriptivo / Text Description"]: # Chapter title
                dict_test_result["Test date"].append("")
                dict_test_result["Module"].append(module)
                dict_test_result["Progress"].append("")
                dict_test_result["Test ID"].append("")
                dict_test_result["SW-0658-REQ"].append(int(df_srs.iloc[i]["ID"][12:]))
                dict_test_result["Test description"].append(df_srs.iloc[i]["Texto Descriptivo / Text Description"])
                dict_test_result["Initial Conditions"].append("")
                dict_test_result["Results"].append("")
                dict_test_result["Status"].append("")
                dict_test_result["Comment"].append("")
                
                dict_test_result_2["Test date"].append("")
                dict_test_result_2["Module"].append(module)
                dict_test_result_2["Progress"].append("")
                dict_test_result_2["Test ID"].append("")
                dict_test_result_2["SW-0658-REQ"].append(int(df_srs.iloc[i]["ID"][12:]))
                dict_test_result_2["Test description"].append(df_srs.iloc[i]["Texto Descriptivo / Text Description"])
                dict_test_result_2["Initial Conditions"].append("")
                dict_test_result_2["Results"].append("")
                dict_test_result_2["Status"].append("")
                dict_test_result_2["Comment"].append("")
            
            elif df_srs.iloc[i]["Categoría / Category"] == 'P': # Requirement
                if "VCU1_PH" in str(df_srs.iloc[i]["VCUx_PH"]): # Of PH1
                    dict_test_result["Test date"].append(date)
                    dict_test_result["Module"].append(module)
                    dict_test_result["Progress"].append(test_id)
                    dict_test_result["Test ID"].append(test_id)
                    dict_test_result["SW-0658-REQ"].append(int(df_srs.iloc[i]["ID"][12:]))
                
                    description = df_srs.iloc[i]["Texto Descriptivo / Text Description"]
                    given_starts = description.find("GIVEN")
                    then_starts = description.find("THEN")
                    test_description = ""
                    if given_starts != 0: # There is a test description
                        test_description = description[:given_starts - 1]
                    initial_conditions = description[given_starts:then_starts]
                    results = description[then_starts + 5:]
                
                    dict_test_result["Test description"].append(test_description)
                    dict_test_result["Initial Conditions"].append(initial_conditions)
                    dict_test_result["Results"].append(results)
                    dict_test_result["Status"].append("In progress")
                    dict_test_result["Comment"].append("")

                    test_id += 1
                    if not at_least_one_req_ph1:
                        at_least_one_req_ph1 = True
                if "VCU2_PH" in str(df_srs.iloc[i]["VCUx_PH"]): # Of PH2
                    dict_test_result_2["Test date"].append(date)
                    dict_test_result_2["Module"].append(module)
                    dict_test_result_2["Progress"].append(test_id_ph2)
                    dict_test_result_2["Test ID"].append(test_id_ph2)
                    dict_test_result_2["SW-0658-REQ"].append(int(df_srs.iloc[i]["ID"][12:]))
                
                    description = df_srs.iloc[i]["Texto Descriptivo / Text Description"]
                    given_starts = description.find("GIVEN")
                    then_starts = description.find("THEN")
                    test_description = ""
                    if given_starts != 0: # There is a test description
                        test_description = description[:given_starts - 1]
                    initial_conditions = description[given_starts:then_starts]
                    results = description[then_starts + 5:]
                
                    dict_test_result_2["Test description"].append(test_description)
                    dict_test_result_2["Initial Conditions"].append(initial_conditions)
                    dict_test_result_2["Results"].append(results)
                    dict_test_result_2["Status"].append("In progress")
                    dict_test_result_2["Comment"].append("")
                    
                    test_id_ph2 += 1
                    if not at_least_one_req_ph2:
                        at_least_one_req_ph2 = True
    if VCU_CH:
        df_test_result = pd.DataFrame(dict_test_result)
        excel_file_name = f"Test report {module} VCU_CH.xlsx"
        # Highlight titles
        styled = df_test_result.style.apply(highlight, axis=None)
        excel_file_name = excel_file_name.replace('/','_')
        print(excel_file_name)
        styled.to_excel(excel_file_name, index = False)
        with pd.ExcelWriter(excel_file_name) as writer:
            sheet_name = "Sheet1"
            styled.to_excel(writer, sheet_name=sheet_name, index = False)
            writer.sheets["Sheet1"].set_column(0,0,10)
            writer.sheets["Sheet1"].set_column(1,1,30)
            writer.sheets["Sheet1"].set_column(4,4,15)
            writer.sheets["Sheet1"].set_column(5,5,60)
            writer.sheets["Sheet1"].set_column(6,7,50)
            writer.sheets["Sheet1"].set_column(8,8,10)
            writer.sheets["Sheet1"].set_column(9,9,12)
    else:
        styled_ph1 = None
        styled_ph2 = None
        if at_least_one_req_ph1:
            df_test_result_ph1 = pd.DataFrame(dict_test_result)
            styled_ph1 = df_test_result_ph1.style.apply(highlight, axis=None)
        if at_least_one_req_ph2:
            df_test_result_ph2 = pd.DataFrame(dict_test_result_2)
            styled_ph2 = df_test_result_ph2.style.apply(highlight, axis=None)
        excel_file_name = f"Test report {module} VCU_PH.xlsx"
        with pd.ExcelWriter(excel_file_name) as writer:
            if styled_ph1:
                sheet_name_1 = "VCU1_PH"
                styled_ph1.to_excel(writer, sheet_name=sheet_name_1, index = False)
                
                writer.sheets["VCU1_PH"].set_column(0,0,10)
                writer.sheets["VCU1_PH"].set_column(1,1,30)
                writer.sheets["VCU1_PH"].set_column(4,4,15)
                writer.sheets["VCU1_PH"].set_column(5,5,60)
                writer.sheets["VCU1_PH"].set_column(6,7,50)
                writer.sheets["VCU1_PH"].set_column(8,8,10)
                writer.sheets["VCU1_PH"].set_column(9,9,12)
            
            if styled_ph2:
                sheet_name_2 = "VCU2_PH"
                styled_ph2.to_excel(writer, sheet_name=sheet_name_2, index = False)
            
                writer.sheets["VCU2_PH"].set_column(0,0,10)
                writer.sheets["VCU2_PH"].set_column(1,1,30)
                writer.sheets["VCU2_PH"].set_column(4,4,15)
                writer.sheets["VCU2_PH"].set_column(5,5,60)
                writer.sheets["VCU2_PH"].set_column(6,7,50)
                writer.sheets["VCU2_PH"].set_column(8,8,10)
                writer.sheets["VCU2_PH"].set_column(9,9,12)
            
if __name__ == "__main__":
    main()