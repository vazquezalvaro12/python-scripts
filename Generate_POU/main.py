import tkinter as tk
from tkinter import ttk
from tkinter import filedialog
from tkinter import messagebox
from tkinter import font
import sys

def read_csv():
    print("Select file.")
    # tk.Tk().withdraw() # prevents an empty tkinter window from appearing
    input_file = filedialog.askopenfilename(title = "Seleccione fichero de variables")
    if input_file:
        print("File selected " + input_file)
    else:
        print("No file selected.")
        return
    content = ""
    try:
        with open(input_file, 'r') as in_f:
            content = in_f.readlines()
    except FileNotFoundError as e:
        print(e)
        return
    except UnicodeDecodeError as e:
        print(e)
        return
    return content

class Main_window:
    def __init__(self):
        self.root = tk.Tk()
        self.root.title('POU GENERATOR')
        self.root.protocol("WM_DELETE_WINDOW", self.on_closing)
        self.background_color = "white"
        self.root.configure(bg=self.background_color)
        self.screen_width = self.root.winfo_screenwidth()
        self.screen_heigh = self.root.winfo_screenheight()
        # Set size and position of variable window
        self.root.geometry(f"{self.screen_width}x{self.screen_heigh-70}+{0}+{0}")
        self.root.resizable(False, False)
        self.root.iconbitmap("Icon.ico")
        self.font = "Gabriola"
        self.font_size = 18
        
        self.selected_instances = {} # dict instance: [[inputs], [inputs_index], [outputs], [outputs_index]]
        self.button_list = [] # list of buttons of the selected instances
 
    
    def create_listbox_instances(self, instances):
        l1 = tk.Label(self.root, text="INSTANCE LIST", font=(self.font, self.font_size), bg=self.background_color)
        l1.place(relx=0, rely=0.025, relwidth=0.2, relheight=0.027)
        var = tk.Variable(value=instances)
        self.listbox = tk.Listbox(self.root, listvariable=var, selectmode=tk.MULTIPLE)
        self.listbox.place(relx=0, rely=0.06, relwidth=0.2, relheight=0.54) # previous relheight=0.94
        self.scrollbar = ttk.Scrollbar(self.root, orient=tk.VERTICAL, command=self.listbox.yview)
        self.listbox["yscrollcommand"] = self.scrollbar.set
        self.scrollbar.place(relx=0.2, rely=0.06, relwidth=0.01, relheight=0.54)
        self.listbox.bind('<<ListboxSelect>>', self.items_selected)
    
    def create_listbox_selected(self):
        l5 = tk.Label(self.root, text="Selected", font=(self.font, self.font_size), bg = self.background_color)
        l5.place(relx=0, rely=0.6, relwidth=0.2, relheight=0.027)
        
        self.selected_inouts = tk.StringVar()
        self.listbox_selected = tk.Listbox(self.root, listvariable=self.selected_inouts)
        self.listbox_selected.configure(state = "disabled")
        self.listbox_selected.place(relx=0, rely=0.65, relwidth=0.2, relheight=0.35)
        self.scrollbar_sel = ttk.Scrollbar(self.root, orient=tk.VERTICAL, command = self.listbox_selected.yview)
        self.listbox_selected["yscrollcommand"] = self.scrollbar_sel.set
        self.scrollbar_sel.place(relx=0.2, rely=0.65, relwidth=0.01, relheight=0.35)
    
    def create_number_inputs_outputs(self):
        l2 = tk.Label(self.root, text="Inputs         Nº:", font=(self.font, self.font_size - 2), bg=self.background_color)
        l2.place(relx=0.25, rely=0.05)
        l3 = tk.Label(self.root, text="Outputs        Nº:", font=(self.font, self.font_size - 2), bg=self.background_color)
        l3.place(relx=0.55, rely=0.05)
        self.entry_inputs = tk.Label(self.root, font=(self.font, self.font_size - 2), bg=self.background_color)
        self.entry_inputs.place(relx=0.305, rely=0.05, relwidth=0.02)
        self.entry_outputs = tk.Label(self.root, font=(self.font, self.font_size - 2), bg=self.background_color)
        self.entry_outputs.place(relx=0.605, rely=0.05, relwidth=0.02)
    
    def create_option_buttons(self):
        self.save_b = ttk.Button(self.root, text="SAVE", command=self.saving_command)
        self.save_b.place(relx=0.85, rely=0.1, relwidth=0.1, relheight=0.05)
        
        self.erase_b = ttk.Button(self.root, text="ERASE", command=self.erase_command)
        self.erase_b.place(relx=0.85, rely=0.2, relwidth=0.1, relheight=0.05)
        
        self.generate_POU_b = ttk.Button(self.root, text="CREATE POU", command=self.create_command)
        self.generate_POU_b.place(relx=0.85, rely=0.3, relwidth=0.1, relheight=0.05)
        
        self.exit_b = ttk.Button(self.root, text="EXIT", command=self.on_closing)
        self.exit_b.place(relx=0.85, rely=0.4, relwidth=0.1, relheight=0.05)
    
    def create_instance_selected(self):
        l4 = tk.Label(self.root, text="Instance selected: ", font=(self.font, self.font_size - 2), bg=self.background_color)
        l4.place(relx=0.7, rely=0.05)
        self.selected_instance_l = tk.Label(self.root, text="", font=(self.font, self.font_size - 2), bg=self.background_color)
        self.selected_instance_l.place(relx=0.77, rely=0.05)
    
    def create_listboxes_variables(self, variables):
        var = tk.Variable(value=variables)
        self.listbox_in = tk.Listbox(self.root, listvariable=var)
        self.listbox_in.place(relx=0.25, rely=0.1, relwidth=0.25, relheight=0.9)
        self.scrollbar_in = ttk.Scrollbar(self.root, orient=tk.VERTICAL, command=self.listbox_in.yview)
        self.listbox_in["yscrollcommand"] = self.scrollbar_in.set
        self.scrollbar_in.place(relx=0.5, rely=0.1, relwidth=0.01, relheight=0.9)
        self.listbox_in.bind("<<ListboxSelect>>", self.items_selected_v_in)
        
        self.listbox_out = tk.Listbox(self.root, listvariable=var)
        self.listbox_out.place(relx=0.55, rely=0.1, relwidth=0.25, relheight=0.9)
        self.scrollbar_out = ttk.Scrollbar(self.root, orient=tk.VERTICAL, command=self.listbox_out.yview)
        self.listbox_out["yscrollcommand"] = self.scrollbar_out.set
        self.scrollbar_out.place(relx=0.8, rely=0.1, relwidth=0.01, relheight=0.9)
        self.listbox_out.bind("<<ListboxSelect>>", self.items_selected_v_out)
    
    def items_selected(self, event):
        # get selected indices
        selected_indices = self.listbox.curselection()
        if not selected_indices:
            return
        # get selected items
        self.selected_instances = {self.listbox.get(i):[[], []] for i in selected_indices}
        for button in self.button_list:
            if button:
                button.place_forget()
                button.destroy()
        self.button_list = []
        pos_x = 0
        inc_x = 1/len(self.selected_instances.keys())
        for instance in self.selected_instances.keys():
            self.button_list.append(tk.Button(self.root, text=instance))
        for button in self.button_list:
            button.place(relx=pos_x, rely=0, relwidth=inc_x)
            button.configure(command=lambda b=button: self.button_command(b))
            pos_x += inc_x
            
    def items_selected_v_in(self, event):
        # get selected indices
        selected_index = self.listbox_in.curselection()
        if not selected_index:
            return
        if self.selected_instance_l.cget('text') == "":
            messagebox.showerror(title="Error", message="Select an instance")
            return
        selected_input = self.listbox_in.get(selected_index)
        if selected_input not in self.selected_instances[self.selected_instance_l.cget('text')][0]:
            self.selected_instances[self.selected_instance_l.cget('text')][0].append(selected_input)
        else:
            self.selected_instances[self.selected_instance_l.cget('text')][0].remove(selected_input)
        self.entry_inputs["text"] = str(len(self.selected_instances[self.selected_instance_l.cget('text')][0]))
        
        # Update listbox selected
        self.selected_inouts.set(self.selected_instances[self.selected_instance_l.cget('text')][0])
    
    def items_selected_v_out(self, event):
        # get selected indices
        selected_index = self.listbox_out.curselection()
        if not selected_index:
            return
        if self.selected_instance_l.cget('text') == "":
            messagebox.showerror(title="Error", message="Select an instance")
            return
        selected_output = self.listbox_out.get(selected_index)
        if selected_output not in self.selected_instances[self.selected_instance_l.cget('text')][1]:
            self.selected_instances[self.selected_instance_l.cget('text')][1].append(selected_output)
        else:
            self.selected_instances[self.selected_instance_l.cget('text')][1].remove(selected_output)
        self.entry_outputs["text"] = str(len(self.selected_instances[self.selected_instance_l.cget('text')][1]))
        
        # Update listbox selected
        self.selected_inouts.set(self.selected_instances[self.selected_instance_l.cget('text')][1])
        
    def create_icon(self):
        self.img = tk.PhotoImage(file="Icon.png")
        self.label_icon = tk.Label(self.root, image=self.img)
        self.label_icon.place(relx = 0.85, rely=0.95)
        
    def on_closing(self):
        if messagebox.askyesno("Exit", "Do you want to quit the application?"):
            self.root.quit()
    
    def saving_command(self):
        instance = self.selected_instance_l.cget('text')
        for button in self.button_list:
            if button.cget('text') == instance:
                button.configure(bg="#33ff77") # green
    
    def erase_command(self):
        instance = self.selected_instance_l.cget('text') 
        for button in self.button_list:
            if button.cget('text') == instance:
                button.configure(bg="#f0f0f0") # default
        self.selected_instances[instance] = [[], []]
        self.entry_inputs["text"] = 0
        self.entry_outputs["text"] = 0
        self.selected_inouts.set([])
    
    def create_command(self):
        if not self.selected_instances:
            messagebox.showerror(title="Error", message="You haven't registered any instances")
        else:
            self.ask_pou_name()
    
    def ask_pou_name(self):
        self.ask_window = tk.Toplevel(self.root)
        self.ask_window.title('Enter POU name')
        # Set size and position of variable window
        self.ask_window.geometry(f"{300}x{50}+{int(self.screen_width/2)-150}+{int(self.screen_heigh/2)-25}")
        self.ask_window.resizable(False, False)
        
        self.entry_pou_name = ttk.Entry(self.ask_window, width = 25)
        self.entry_pou_name.pack()
        
        accept_button = ttk.Button(self.ask_window, text="Accept", command=self.accept_command).pack()
      
    def accept_command(self):
        self.pou_name = self.entry_pou_name.get()
        self.ask_window.destroy()
        generate_POU(self.pou_name, self.selected_instances)
        
        
    def button_command(self, button):
        instance = button.cget('text')
        self.selected_instance_l["text"] = instance
        number_inputs = len(self.selected_instances[instance][0])
        number_outputs = len(self.selected_instances[instance][1])
        self.entry_inputs["text"] = number_inputs
        self.entry_outputs["text"] = number_outputs
        
    def run(self):
        self.root.mainloop()

# Function to generate POU based on a dictionary of instances which has the form:
#   instance : [[inputs], [outputs]]
def generate_POU(pou_name, instances):
    with open(pou_name+'.stf', 'w') as out_f:
        out_f.write("PROGRAM " + pou_name + '\n')
        out_f.write("#info= FBD\n")
        nbid = calculate_nbid(instances) # total number of ids
        out_f.write("@@NBID=" + str(nbid) + '\n')
        
        isagraf_font = font.Font(family="Arial", size=10) # declare isagraf font
        
        # Draw instances
        instance_y = {}
        current_id = 1
        o_x = 300
        o_y = 40
        inc_y = 0
        offset_y = 30
        for instance in instances.keys():
            Sx = calculate_Sx(isagraf_font, instance)
            Sy = calculate_Sy(instances[instance])
            Py = o_y + inc_y
            out_f.write(f"@BOX:{str(current_id)},P=({str(o_x)},{str(Py)})," + 
                        f"S=({str(Sx)},{str(Sy)})," + 
                        f"C=({str(len(instances[instance][0]))},{str(len(instances[instance][1]))})," + 
                        f"X={instance}\n")
            out_f.write(f"T={instance}\n")
            current_id += len(instances[instance][0]) + len(instances[instance][1]) + 1
            inc_y += Sy + offset_y
            instance_y[instance] = Py
        
        # Draw variables and get arc coordinates
        current_id = 0
        gap_var_instance = 6
        arc_position = []
        for instance in instances.keys():
            current_id += 1
            Sx_instance = calculate_Sx(isagraf_font, instance)
            Sy_instance = calculate_Sy(instances[instance])
            inputs = instances[instance][0]
            outputs = instances[instance][1]
            if len(inputs) >= len(outputs):
                y = 4 + instance_y[instance]
                inc_y = 0
                arc_x = o_x - 3
                for i in inputs:
                    current_id += 1
                    Sx = calculate_Sx(isagraf_font, i)
                    Px = o_x - Sx - gap_var_instance
                    Py = y + inc_y
                    arc_y = Py + 2
                    arc_position.append((arc_x, arc_y))
                    out_f.write(f"@VAR:{str(current_id)},P=({str(Px)},{str(Py)})," + 
                                f"S=({str(Sx)},5),C=(1,1),X={i}\n")
                    inc_y += 6
                    
                Px = o_x + Sx_instance + gap_var_instance
                y = instance_y[instance] + int(Sy_instance / (len(outputs) + 1) - 2)
                inc_y = 0
                a = int(Sy_instance / (len(outputs) + 1))
                arc_x = o_x + Sx_instance + 3
                for o in outputs:
                    current_id += 1
                    Sx = calculate_Sx(isagraf_font, o)
                    Py = y + inc_y
                    arc_y = Py + 2
                    arc_position.append((arc_x, arc_y))
                    out_f.write(f"@VAR:{str(current_id)},P=({str(Px)},{str(Py)})," +
                                f"S=({str(Sx)},5),C=(1,1),X={o}\n")
                    inc_y += a
            else:
                y = instance_y[instance] + int(Sy_instance / (len(inputs) + 1) - 2)
                inc_y = 0
                a = int(Sy_instance / (len(inputs) + 1))
                arc_x = o_x - 3
                for i in inputs:
                    current_id += 1
                    Sx = calculate_Sx(isagraf_font, i)
                    Px = o_x - Sx - gap_var_instance
                    Py = y + inc_y
                    arc_y = Py + 2
                    arc_position.append((arc_x, arc_y))
                    out_f.write(f"@VAR:{str(current_id)},P=({str(Px)},{str(Py)})," +
                                f"S=({str(Sx)},5),C=(1,1),X={i}\n")
                    inc_y += a
                
                Px = o_x + Sx_instance + gap_var_instance
                y = 4 + instance_y[instance]
                inc_y = 0
                arc_x = o_x + Sx_instance + 3
                for o in outputs:
                    current_id += 1
                    Sx = calculate_Sx(isagraf_font, o)
                    Py = y + inc_y
                    arc_y = Py + 2
                    arc_position.append((arc_x, arc_y))
                    out_f.write(f"@VAR:{str(current_id)},P=({str(Px)},{str(Py)})," +
                                f"S=({str(Sx)},5),C=(1,1),X={o}\n")
                    inc_y += 6
        # Draw arcs
        current_id = 0
        idx_arc_pos = 0
        for instance in instances.keys():
            inputs = instances[instance][0]
            outputs = instances[instance][1]
            current_id += 1
            instance_id = current_id
            idx_instance = 0 # Idx of inputs of instance 
            for i in inputs:
                current_id += 1
                Px = arc_position[idx_arc_pos][0]
                Py = arc_position[idx_arc_pos][1]
                out_f.write(f"@ARC:D=0,Z=(0,0),F=({current_id},0)," + 
                            f"T=({instance_id},{idx_instance})\n" +
                            f"P=({Px}.{Py};{Px}.{Py})\n")
                idx_instance += 1
                idx_arc_pos += 1
                
            idx_instance = 0 # Idx of outputs of instance
            for o in outputs:
                current_id += 1
                Px = arc_position[idx_arc_pos][0]
                Py = arc_position[idx_arc_pos][1]
                out_f.write(f"@ARC:D=0,Z=(0,0),F=({instance_id},{idx_instance})," + 
                            f"T=({current_id},0)\n" +
                            f"P=({Px}.{Py};{Px}.{Py})\n")
                idx_instance += 1
                idx_arc_pos += 1
      
        out_f.write("#end_info\nEND_PROGRAM")
        messagebox.showinfo(title="POU Generation", message="POU generated correctly")
        
# Calculates number of ids
def calculate_nbid(instances):
    nbid = 0
    for key in instances.keys():
        nbid += 1
        nbid += len(instances[key][0]) + len(instances[key][1])
    return nbid

# Calculate Sx for variables and instances
def calculate_Sx(f, name):
    length = int(0.465 * int(f.measure(name)) - 20)
    if length < 25:
        return 25
    else:
        return length

# Calculate Sy for instances
def calculate_Sy(inputs_outputs):
    return (max(len(inputs_outputs[0]), len(inputs_outputs[1])) - 1) * 6 + 12

window = Main_window()

def configure_window(window, instances, variables):
    window.create_listbox_instances(instances)
    window.create_listbox_selected()
    window.create_number_inputs_outputs()
    window.create_listboxes_variables(variables)
    window.create_instance_selected()
    window.create_option_buttons()
    window.create_icon()

def main():
    content = read_csv()
    variables = []
    instances = []
    for line in content[1:]:
        word = line.split(',')[2]
        if word[:2] == "F_" and word not in instances: # is instance and not present
            instances.append(word)
        elif word[:2] != "E_" and word[:2] != "K_" and word not in variables: # not an enum neither a constant
            variables.append(word)
    variables.sort()
    instances.sort()
    configure_window(window, instances, variables)
    window.run()

if __name__ == "__main__":
    main()
    
